
---
License:
	This Source Code Form is subject to the terms of the Mozilla Public License,
	v. 2.0. If a copy of the MPL was not distributed with this file, You can
	obtain one at http://mozilla.org/MPL/2.0/.
---

> module ANSI.Term.Escape

> import Data.Fin
> import ANSI.Term.Type
> %default total

`escapeSequence` is the principal method of turning the data structures in
[ANSI.Term.Type](ANSI.Term.Type.html)
into actual terminal behaviour.

In order to use escape codes,
it is enough to simply print them to the terminal.
```
putStrLn $ (escapeSequence [SetColor8 Foreground Red]) ++ "Red!"
```

> public export
> interface EscapeSequence a where
>   escapeSequence : a -> String

> implicit
> toIntColor8 : (c: Color8) -> Int
> toIntColor8 c = case c of
>   Black => 0
>   Red => 1
>   Green => 2
>   Yellow => 3
>   Blue => 4
>   Magenta => 5
>   Cyan => 6
>   White => 7

Small utility to actually build the escape sequence strings.

Technically all arguments could be `Nat` instead of `Int`,
and they were, once - but were changed in an attempt to improve performance
at the Idris REPL. (I don't think it did much.)

> private
> csiCode : (args: List Int) -> (command: String) -> String
> csiCode args cmd =
>   "\ESC[" ++ concat (intersperse ";" $ map show args) ++ cmd

= Instances for `EscapeSequence`:

== `List SGR`
We don't bother with an `EscapeSequence SGR` instance,
because the utility of being able to change one display property at a time
is probably not worth the extra work.

> export
> EscapeSequence (List SGR) where
>   escapeSequence s = csiCode (join (map toIntSGR s)) "m"
>     where
>     finToInt : Fin n -> Int
>     finToInt = fromInteger . finToInteger
>     fgbg : FGBG -> Int -> Int -> Int
>     fgbg x f b = case x of
>       Foreground => f
>       Background => b
>     rgb6 : Fin 6 -> Fin 6 -> Fin 6 -> Int
>     rgb6 r g b = (36 * finToInt r) + (6 * finToInt g) + (finToInt b)
>     color256 : FGBG -> Int -> List Int
>     color256 l c = [fgbg l 38 48, 5, c]
>     toIntSGR : SGR -> List Int
>     toIntSGR x = case x of
>       Reset => [0]
>       SetIntensity Nothing => [22]
>       SetIntensity (Just Bold) => [1]
>       SetIntensity (Just Faint) => [2]
>       SetItalic True => [3]
>       SetItalic False => [23]
>       SetUnderline Nothing => [24]
>       SetUnderline (Just SingleUnderline) => [4]
>       SetUnderline (Just DoubleUnderline) => [21]
>       SetBlink Nothing => [25]
>       SetBlink (Just Slow) => [5]
>       SetBlink (Just Rapid) => [6]
>       SetVisible True => [28]
>       SetVisible False => [8]
>       SetSwapFGBG True => [27]
>       SetSwapFGBG False => [7]
>       SetColor8 l c => [(fgbg l 30 40) + c]
>       SetColor256 l (Dull c) => color256 l c
>       SetColor256 l (Vivid c) => color256 l $ 8 + c
>       SetColor256 l (RGB6 r g b) => color256 l $ 16 + rgb6 r g b
>       SetColor256 l (Grayscale24 n) => color256 l $ 232 + finToInt n
>       -- SetColor l (RawColor n) => [(fgbg l) + 8, 5, n]

== `CursorMovement`

> export
> EscapeSequence CursorMovement where
>   escapeSequence s = uncurry csiCode $ case s of
>     CursorUp n => ([cast n], "A")
>     CursorDown n => ([cast n], "B")
>     CursorRight n => ([cast n], "C")
>     CursorLeft n => ([cast n], "D")
>     CursorNextLine n => ([cast n], "E")
>     CursorPreviousLine n => ([cast n], "F")
>     CursorToColumn n => ([cast n], "G")
>     CursorPosition x y => ([cast x, cast y], "H")

== `CursorVisible`

> export
> EscapeSequence CursorVisible where
>   escapeSequence s = case s of
>     -- Cheating with private codes
>     ShowCursor => csiCode [] "?25h"
>     HideCursor => csiCode [] "?25l"

== `Scroll`

> export
> EscapeSequence Scroll where
>   escapeSequence s = uncurry csiCode $ case s of
>     ScrollUp n => ([cast n], "S")
>     ScrollDown n => ([cast n], "T")

== `Clear`

> export
> EscapeSequence Clear where
>   escapeSequence s = uncurry csiCode $ case s of
>     ClearScreen ClearFromCursor => ([0], "J")
>     ClearScreen ClearToCursor => ([1], "J")
>     ClearScreen ClearAll => ([2], "J")
>     ClearLine ClearFromCursor => ([0], "K")
>     ClearLine ClearToCursor => ([1], "K")
>     ClearLine ClearAll => ([2], "K")


