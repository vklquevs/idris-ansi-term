
---
License:
	This Source Code Form is subject to the terms of the Mozilla Public License,
	v. 2.0. If a copy of the MPL was not distributed with this file, You can
	obtain one at http://mozilla.org/MPL/2.0/.
---

> module ANSI.Term.Type

Data types corresponding to fairly widely-supported
[ANSI escape codes](<https://en.wikipedia.org/wiki/ANSI_escape_code>).

> import Data.Fin
> %default total
> %access public export

=== Style

Text attributes for [Set Graphic Rendition][SGR] sequences.

> data Intensity = Bold | Faint
> data FGBG = Foreground | Background
> data BlinkSpeed = Rapid | Slow
> data Underline = SingleUnderline | DoubleUnderline


The 8 original colors should be supported by all color terminals.

> data Color8 = Black | Red | Green | Yellow | Blue | Magenta | Cyan | White

256-color codes. IDO-8613-3 24-bit color is not yet implemented.

> data Color256
>   = Dull Color8
>   | Vivid Color8
>   | RGB6 (Fin 6) (Fin 6) (Fin 6)
>   | Grayscale24 (Fin 24)
>   -- | RawColor Bits8


=== SGR

Set Graphic Rendition sequences modify text style.

> data SGR
>   = Reset -- TODO also ResetColor?
>   | SetIntensity (Maybe Intensity)
>   | SetItalic Bool
>   | SetUnderline (Maybe Underline)
>   | SetBlink (Maybe BlinkSpeed)
>   | SetVisible Bool
>   | SetSwapFGBG Bool
>   | SetColor8 FGBG Color8
>   | SetColor256 FGBG Color256

=== Movement

Move the cursor.

> data CursorMovement
>   = CursorUp Nat
>   | CursorDown Nat
>   | CursorRight Nat
>   | CursorLeft Nat
>   | CursorNextLine Nat
>   | CursorPreviousLine Nat
>   | CursorToColumn Nat
>   | CursorPosition Nat Nat

Show/hide the cursor. Not widely supported.

> data CursorVisible
>   = ShowCursor
>   | HideCursor

Scroll the screen.

> data Scroll
>   = ScrollUp Nat
>   | ScrollDown Nat

=== Clear

> data ClearMethod
>   = ClearFromCursor
>   | ClearToCursor
>   | ClearAll

> data Clear
>   = ClearScreen ClearMethod
>   | ClearLine ClearMethod



