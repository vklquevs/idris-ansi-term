
{-
 - This Source Code Form is subject to the terms of the Mozilla Public License,
 - v. 2.0. If a copy of the MPL was not distributed with this file, You can
 - obtain one at http://mozilla.org/MPL/2.0/.
 -}


||| Data types corresponding to fairly widely-supported ANSI escape sequences.
|||
||| See https://en.wikipedia.org/wiki/ANSI_escape_code

module ANSI.Term.Type

import Data.Fin

%default total
%access public export

data Intensity = Bold | Faint

data FGBG = Foreground | Background

data BlinkSpeed = Rapid | Slow

data Underline = SingleUnderline | DoubleUnderline

||| 8 original colors, black = 0 .. white = 7
data Color8 = Black | Red | Green | Yellow | Blue | Magenta | Cyan | White

||| IDO-8613-3 24-bit color is not yet implemented.
data Color256
  = Dull Color8
  | Vivid Color8
  | RGB6 (Fin 6) (Fin 6) (Fin 6)
  | Grayscale24 (Fin 24)
  -- | RawColor Bits8

||| Set Graphic Rendition
data SGR
  = Reset -- TODO also ResetColor?
  | SetIntensity (Maybe Intensity)
  | SetItalic Bool
  | SetUnderline (Maybe Underline)
  | SetBlink (Maybe BlinkSpeed)
  | SetVisible Bool
  | SetSwapFGBG Bool
  | SetColor8 FGBG Color8
  | SetColor256 FGBG Color256

data CursorMovement
  = CursorUp Nat
  | CursorDown Nat
  | CursorRight Nat
  | CursorLeft Nat
  | CursorNextLine Nat
  | CursorPreviousLine Nat
  | CursorToColumn Nat
  | CursorPosition Nat Nat

data CursorVisible
  = ShowCursor
  | HideCursor

data Scroll
  = ScrollUp Nat
  | ScrollDown Nat

data ClearMethod
  = ClearFromCursor
  | ClearToCursor
  | ClearAll

data Clear
  = ClearScreen ClearMethod
  | ClearLine ClearMethod

