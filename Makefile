
SOURCE_DIRS = ANSI Test

INDEX_MD = README.md TOC.md

LITERATE_SRC = find ${SOURCE_DIRS} -name '*.lidr'

publish: clean doc

# Luckily, Idris looks a lot like Haskell.
MAKE_DOC = pandoc -c style.css -f markdown+lhs -s -t html

.PHONY: clean
clean:
	rm -rf doc

doc: $(shell ${LITERATE_SRC}) ${HTML_CSS} ${INDEX_MD} $(wildcard docsrc/*)
	@mkdir -p $@
	@${LITERATE_SRC} | while read lidr; do \
		echo "pandoc $$lidr -> doc/" ;\
		ofile=$$(basename $$(echo "$$lidr" | tr / .) .lidr).html ;\
		${MAKE_DOC} --toc $$lidr > $@/$$ofile ;\
	done
	pandoc -c style.css -s ${INDEX_MD} -t html > doc/index.html
	cp docsrc/* doc/


