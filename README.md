
# ansi-term

Basic ANSI terminal escape codes for Idris.
Inspired by `ansi-terminal` on Hackage.
No external dependencies.
MPLv2.

### To do

- More escapes
- More documentation
- Nice SGR formatting DSL, e.g. `[red [bold "A", bg blue "B"]]`

