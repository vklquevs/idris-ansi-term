
module Test.AnsiTerm

import Data.Fin
import ANSI.Term

%access export

private
enumFin : %static (n:Nat) -> List (Fin (S n))
enumFin n = enumFromTo FZ last

private
enumRGB6 : List (Fin 6)
enumRGB6 = enumFin 5

private
enumGray24 : List (Fin 24)
enumGray24 = enumFin 23

private
colors8 : List Color8
colors8 = [ Black, Red, Green, Yellow, Blue, Magenta, Cyan, White ]

private
demoString : List SGR -> String
demoString sgrs =
  concat $
    (map (\s => escapeSequence [s] ++ "  ") sgrs)
    ++ [escapeSequence [Reset]]

private
grayscale24 : String
grayscale24 =
  demoString $ map (SetColor256 Background . Grayscale24) enumGray24

colorDemo : IO ()
colorDemo = do
    putStrLn "-- Color demo"
    putStrLn ""
    putStrLn "Standard colors"
    putStrLn $ demoString $ map (SetColor8 Background) colors8
    resetSGR
    putStrLn "256-color standard colors"
    putStrLn $ demoString $ map (SetColor256 Background . Dull) colors8
    putStrLn $ demoString $ map (SetColor256 Background . Vivid) colors8
    putStrLn ""
    putStrLn "6x6x6 color cube"
    for_ enumRGB6 $ \r =>
      putStrLn $ demoString $ concat $
      flip map enumRGB6 $ \g =>
        flip map enumRGB6 $ \b =>
          SetColor256 Background $ RGB6 r g b
    putStrLn ""
    putStrLn "Grayscale 24"
    putStrLn grayscale24
  where
  resetSGR : IO ()
  resetSGR = putStrLn $ escapeSequence [Reset]
  color256 : Color256 -> IO ()
  color256 c = putStr $ (escapeSequence [SetColor256 Background c]) ++ "  "

